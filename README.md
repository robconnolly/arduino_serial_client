# Serial Network Client for Arduino

Provides an implementation of the Arduino Client interface over serial. This can
be used to forward a network connection via the serial port.

## Installation

Now installable from [platformio](http://platformio.org/lib/show/297/SerialClient):

```shell
$ platformio lib install 297
```

## Example

One example is the Arduino [MQTT client](http://pubsubclient.knolleary.net/),
which expects a client interface in it's constructor. To forward the MQTT connection
over the serial port, we could do the following:

```C++
#include "Arduino.h"
#include "SerialClient.h"
#include "PubSubClient.h"

byte server[] = { 127, 0, 0, 1 }; // not actually needed for serial PTP link

void pubSubCallback(char* topic, byte* payload, unsigned int length);

SerialClient serClient(Serial);
PubSubClient client(server, 1883, pubSubCallback, serClient);

void setup(void)
{
    Serial.begin(115200);
    ...
}

...
```

The `client` object can then be used as normal.

On the host computer [socat](http://www.dest-unreach.org/socat/) can be used
to forward the connection from the serial port to the MQTT broker.

```shell
$ socat file:/dev/ttyUSB0,echo=0,b115200,raw tcp-connect:localhost:1883
```
